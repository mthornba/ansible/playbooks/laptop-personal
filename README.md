Laptop - Personal Ansible Playbook
==================================

Prep
----

After a fresh OS install, install all updates, then create a snapshot.

Create a backup using Timeshift
```
sudo apt update
sudo apt install timeshift
sudo timeshift --create --comments "Clean System"
```

Install Ansible and git
```
sudo apt install ansible git
```

Clone this repo
```
git clone https://gitlab.com/mthornba/ansible/playbooks/laptop-personal.git ~/Code/ansible/playbooks/laptop-personal
```

Run Ansible
-----------

Install Plugins
```
ansible-galaxy collection install community.general
```

Download roles
```
ansible-galaxy install --roles-path ~/.ansible/roles -r requirements.yml
```

Run playbook
```
ansible-playbook laptop.yml -K
```

Testing Role Changes
--------------------

Clone roles locally and set Ansible Role Path
```
export ANSIBLE_ROLES_PATH=~/Code/ansible/roles
ansible-playbook laptop.yml -K
```
